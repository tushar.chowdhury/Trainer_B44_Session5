<?php

// Boolean example started form here
$decision = true;
// if(expression == true) {  do this }
if($decision){
    echo "The decision is True<br>";
}

$decision = false;
// if(expression == true) {  do this }
if($decision){
    echo "The decision is False<br>";
}
// Boolean example ended here


// Interger example started form here

$value  =  100 ;
echo $value;
echo "<br>";

// Integer example ended here


// Float/Double example started form here

$value  =  35.86;
echo $value;
echo "<br>";

//  Float/Double example ended here


// string example start here
   $var = 100;
   $string1 = 'This is single quoted string $var <br>';
   $string2 = "This is double quoted string $var <br>";
   echo $string1.$string2;

$heredocString=<<<BITM
    this is  heredoc exmaple line1 $var <br>
    this is  heredoc exmaple line2 $var <br>
BITM;

$nowdocString=<<<'BITM'
    this is  nowdoc exmaple line1 $var <br>
    this is  nowdoc exmaple line2 $var <br>
BITM;

echo $heredocString . "<br> <br>" . $nowdocString;
// string example end here

$arr = array(1,2,3,4,5,6,7,8,9,10);
print_r($arr);

echo "<br>";

$arr = array("BMW","TOYOTA","NISSAN","FORD","FERARI","MARUTI");
print_r($arr);

echo "<br>";

$ageArray = array("Arif"=>30,"Moynar Ma"=>45, "Rahim"=>25);
print_r($ageArray);

echo "<br>";

echo "The age of Moynar Ma is " . $ageArray["Moynar Ma"];

echo "<br>";




